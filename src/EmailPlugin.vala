/*-
 * Copyright (c) 2016-2017 elementary LLC. (https://elementary.io)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authored by: Corentin Noël <corentin@elementary.io>
 */

public class GSignond.MailPlugin : GLib.Object, GSignond.Plugin {
    public string type { owned get { return "mail"; } }
    public string[] mechanisms { owned get { return {"mail", null}; } }

    public void cancel () {
        var signond_error = new GSignond.Error.SESSION_CANCELED ("Session canceled");
        error (signond_error);
    }

    public void request_initial (GSignond.SessionData session_data, GSignond.Dictionary token_cache, string mechanism) {
        var username = session_data.get_username ();
        var secret = session_data.get_secret ();
        if (secret == null) {
            var response = new GSignond.SessionData.from_variant (token_cache.to_variant ());
            if (username != null) {
                response.set_username (username);
            }

            response.set_secret (secret);
            response_final (response);
            return;
        }

        var user_action_data = new GSignond.SignonuiData ();
        if (username != null) {
            user_action_data.set_query_username (true);
        } else {
            user_action_data.set_query_username (false);
            user_action_data.set_username (username);
        }

        user_action_data.set_boolean ("AskEmailSettings", true);
        user_action_data.set_query_password (true);
        user_action_required (user_action_data);
    }

    public void request (GSignond.SessionData session_data) {
        var err = new GSignond.Error.WRONG_STATE ("Email plugin doesn't support request");
        error (err); 
    }

    public void user_action_finished (GSignond.SignonuiData ui_data) {
        GSignond.SignonuiError query_error;
        if (ui_data.get_query_error (out query_error) == false) {
            var err = new GSignond.Error.USER_INTERACTION ("user_action_finished did not return an error value");
            error (err);
            return;
        }

        if (query_error == GSignond.SignonuiError.NONE) {
            var response = new GSignond.SessionData ();
            response.set_string ("ImapUser", ui_data.get_string ("ImapUser"));
            response.set_string ("ImapPassword", ui_data.get_string ("ImapPassword"));
            response.set_string ("ImapServer", ui_data.get_string ("ImapServer"));
            var imap_port_var = ui_data.get ("ImapPort");
            if (imap_port_var != null) {
                response.set ("ImapPort", imap_port_var);
            }

            response.set_string ("ImapSecurity", ui_data.get_string ("ImapSecurity"));
            response.set_string ("SmtpUser", ui_data.get_string ("SmtpUser"));
            response.set_string ("SmtpPassword", ui_data.get_string ("SmtpPassword"));
            response.set_string ("SmtpServer", ui_data.get_string ("SmtpServer"));
            var smtp_port_var = ui_data.get ("SmtpPort");
            if (smtp_port_var != null) {
                response.set ("SmtpPort", smtp_port_var);
            }

            response.set_string ("SmtpSecurity", ui_data.get_string ("SmtpSecurity"));
            response.set_username (ui_data.get_string ("ImapUser"));
            response.set_secret (ui_data.get_string ("ImapPassword"));
            store (response);
            response_final (response);
        } else if (query_error == GSignond.SignonuiError.CANCELED) {
            var err = new GSignond.Error.SESSION_CANCELED ("Session canceled");
            error (err);
        } else {
            var err = new GSignond.Error.USER_INTERACTION ("userActionFinished error: %d", query_error);
            error (err);
        }
    }

    public void refresh (GSignond.SignonuiData session_data) {
        refreshed (session_data);
    }
}
